# test-backend

# Dúvidas

1. Enviar email para: <pablosousa@startstudio.com.br>

2. [LastFm](https://www.last.fm/api/show/track.search)

3. Api Key LastFm: aefa54108190e9c1d8dd093e5f628a21

# Teste de Codificação

Criar API para cadastrar musicas com dados do LastFM:

1. Fazer Upload para API Pública. (Não precisa ter login ou autenticação).

2. A partir do upload feito, fazer busca na API do LastFM para buscar dados da Música (Artista, Titulo, Ano, Album, Link da Capa)

3. Salvar dados retornados em um Banco de Dados Mysql

4. Por favor criar arquivo README.md detalhando instruções de como iniciar e rodar o projeto passo à passo.

5. Utilizar PHP7+, Docker

5. Utilizar Framework CakePHP3 (preferencialemnte)

**Não recomendamos que leva mais de 1 semana para fazer essa tarefa.**

# Exemplo

Ex de Rotas:

1. startstudio/api/v1/upload    POST  [faz o upload de um arquivo, e salva as informações]

2. startstudio/api/v1/list      GET     [lista todos arquivos cadastrados com suas informações]

# Criterio de Avaliação

1. Uso de Orientação a Objetos

2. Design Pattern

3. Seguir recomendações Psr-1, Psr-2, para design de código

4. Seguir padrões RestFull

5. Habilidade de documentação

# Entrega

1. Criar um repositorio no seu GitHub, Bitbucket ou GitLab e nos enviar o link.